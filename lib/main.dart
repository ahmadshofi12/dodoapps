import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

import 'package:get/get.dart';
import 'package:http_proxy/http_proxy.dart';

import 'app/routes/app_pages.dart';
// if (Platform.isAndroid) {
//   await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
// }

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  HttpProxy proxy = await HttpProxy.createHttpProxy();
  proxy.host = "http://ec2-18-138-34-218.ap-southeast-1.compute.amazonaws.com/";
  proxy.port = "3128";
  HttpOverrides.global = proxy;
  await Firebase.initializeApp();
  if (Platform.isAndroid) {
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
  }

  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: "Application",
      debugShowCheckedModeBanner: false,
      initialRoute: AppPages.INITIAL,
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
      ),
      getPages: AppPages.routes,
    );
  }
}
