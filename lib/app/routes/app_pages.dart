import 'package:get/get.dart';

import '../modules/add_child/bindings/add_child_binding.dart';
import '../modules/add_child/views/add_child_view.dart';
import '../modules/browser_surfior/bindings/browser_surfior_binding.dart';
import '../modules/browser_surfior/views/browser_surfior_view.dart';
import '../modules/chart/bindings/chart_binding.dart';
import '../modules/chart/views/chart_view.dart';
import '../modules/choose_child_name/bindings/choose_child_name_binding.dart';
import '../modules/choose_child_name/views/choose_child_name_view.dart';
import '../modules/choose_user/bindings/choose_user_binding.dart';
import '../modules/choose_user/views/choose_user_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/list_child/bindings/list_child_binding.dart';
import '../modules/list_child/views/list_child_view.dart';
import '../modules/login_move_toReportior/bindings/login_move_to_reportior_binding.dart';
import '../modules/login_move_toReportior/views/login_move_to_reportior_view.dart';
import '../modules/login_parents/bindings/login_parents_binding.dart';
import '../modules/login_parents/views/login_parents_view.dart';
import '../modules/register_parent/bindings/register_parent_binding.dart';
import '../modules/register_parent/views/register_parent_view.dart';
import '../modules/reportior/bindings/reportior_binding.dart';
import '../modules/reportior/views/reportior_view.dart';
import '../modules/settings/bindings/settings_binding.dart';
import '../modules/settings/views/settings_view.dart';
import '../modules/splash_screen/bindings/splash_screen_binding.dart';
import '../modules/splash_screen/views/splash_screen_view.dart';
import '../modules/surfior/bindings/surfior_binding.dart';
import '../modules/surfior/views/surfior_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SPLASH_SCREEN;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.REGISTER_PARENT,
      page: () => const RegisterParentView(),
      binding: RegisterParentBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN_PARENTS,
      page: () => const LoginParentsView(),
      binding: LoginParentsBinding(),
    ),
    GetPage(
      name: _Paths.CHOOSE_USER,
      page: () => const ChooseUserView(),
      binding: ChooseUserBinding(),
    ),
    GetPage(
      name: _Paths.CHOOSE_CHILD_NAME,
      page: () => const ChooseChildNameView(),
      binding: ChooseChildNameBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN_MOVE_TO_REPORTIOR,
      page: () => const LoginMoveToReportiorView(),
      binding: LoginMoveToReportiorBinding(),
    ),
    GetPage(
      name: _Paths.LIST_CHILD,
      page: () => const ListChildView(),
      binding: ListChildBinding(),
    ),
    GetPage(
      name: _Paths.ADD_CHILD,
      page: () => const AddChildView(),
      binding: AddChildBinding(),
    ),
    GetPage(
      name: _Paths.REPORTIOR,
      page: () => ReportiorView(),
      binding: ReportiorBinding(),
    ),
    GetPage(
      name: _Paths.SURFIOR,
      page: () => SurfiorView(),
      binding: SurfiorBinding(),
    ),
    GetPage(
      name: _Paths.SETTINGS,
      page: () => const SettingsView(),
      binding: SettingsBinding(),
    ),
    GetPage(
      name: _Paths.SPLASH_SCREEN,
      page: () => const SplashScreenView(),
      binding: SplashScreenBinding(),
    ),
    GetPage(
      name: _Paths.CHART,
      page: () => const ChartView(),
      binding: ChartBinding(),
    ),
    GetPage(
      name: _Paths.BROWSER_SURFIOR,
      page: () => const BrowserSurfiorView(),
      binding: BrowserSurfiorBinding(),
    ),
  ];
}
