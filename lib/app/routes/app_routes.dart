part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const REGISTER_PARENT = _Paths.REGISTER_PARENT;
  static const LOGIN_PARENTS = _Paths.LOGIN_PARENTS;
  static const CHOOSE_USER = _Paths.CHOOSE_USER;
  static const CHOOSE_CHILD_NAME = _Paths.CHOOSE_CHILD_NAME;
  static const LOGIN_MOVE_TO_REPORTIOR = _Paths.LOGIN_MOVE_TO_REPORTIOR;
  static const LIST_CHILD = _Paths.LIST_CHILD;
  static const ADD_CHILD = _Paths.ADD_CHILD;
  static const REPORTIOR = _Paths.REPORTIOR;
  static const SURFIOR = _Paths.SURFIOR;
  static const SETTINGS = _Paths.SETTINGS;
  static const SPLASH_SCREEN = _Paths.SPLASH_SCREEN;
  static const CHART = _Paths.CHART;
  static const BROWSER_SURFIOR = _Paths.BROWSER_SURFIOR;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const REGISTER_PARENT = '/register-parent';
  static const LOGIN_PARENTS = '/login-parents';
  static const CHOOSE_USER = '/choose-user';
  static const CHOOSE_CHILD_NAME = '/choose-child-name';
  static const LOGIN_MOVE_TO_REPORTIOR = '/login-move-to-reportior';
  static const LIST_CHILD = '/list-child';
  static const ADD_CHILD = '/add-child';
  static const REPORTIOR = '/reportior';
  static const SURFIOR = '/surfior';
  static const SETTINGS = '/settings';
  static const SPLASH_SCREEN = '/splash-screen';
  static const CHART = '/chart';
  static const BROWSER_SURFIOR = '/browser-surfior';
}
