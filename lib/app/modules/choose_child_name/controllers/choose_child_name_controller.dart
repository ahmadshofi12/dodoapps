// ignore_for_file: prefer_typing_uninitialized_variables

import 'dart:convert';
import 'dart:developer';
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dodo_mobile/app/data/models/child.dart';
import 'package:dodo_mobile/app/utils/constanta.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class ChooseChildNameController extends GetxController {
  final _endPoint = "child/get-user";
  var models;
  // String? token;
  // void getToken() async {
  //   token = await FirebaseMessaging.instance.getToken();
  //   log("TOKEN FIREBASE");
  //   final prefs = await SharedPreferences.getInstance();
  //   await prefs.setString('token_firebase', token!);
  //   log(token.toString());
  // }

  sendDataToFirebase(String name) async {
    final box = GetStorage();
    final prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('token_firebase');
    final data = box.read('dataParent');
    String emailParent = data["email"];
    CollectionReference userParent =
        FirebaseFirestore.instance.collection("user_parent");

    return userParent
        .add({
          'emailParent': emailParent,
          'childName': name,
          'token': token,
        })
        .then((value) => log("Success Save Data"))
        .catchError((err) => log("Failed Add Data"));
  }

  Future<List<Child>> getChilds() async {
    final box = GetStorage();
    final data = box.read("dataParent") as Map<String, dynamic>;
    String accessToken = data["token"];

    try {
      var response = await http.get(
          Uri.parse(
            baseUrl + _endPoint,
          ),
          headers: {'Authorization': 'Bearer $accessToken'});
      // print(response.body);
      if (response.statusCode != 200) {
        throw "error1";
      }

      var data = json.decode(response.body)["data"] as List<dynamic>;
      models = Child.fromJsonList(data);
      // print(models);
    } catch (err) {
      log(err.toString());
    }
    return models;
  }

  @override
  void onInit() async {
    await GetStorage.init();
    super.onInit();
  }
}
