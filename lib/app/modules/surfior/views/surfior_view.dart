import 'dart:developer';

import 'package:dodo_mobile/app/data/models/child.dart';
import 'package:dodo_mobile/app/modules/surfior/views/browserSurfior.dart';
import 'package:dodo_mobile/app/routes/app_pages.dart';
import 'package:dodo_mobile/app/utils/size_config.dart';
import 'package:dodo_mobile/app/utils/style.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/surfior_controller.dart';

class SurfiorView extends GetView<SurfiorController> {
  const SurfiorView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Child child = Get.arguments;
    log(child.name.toString());
    log(child.id.toString());
    log(child.idParent.toString());
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
        body: SizedBox(
          width: Get.width,
          height: Get.height,
          child: Stack(
            children: [
              Positioned.fill(
                child: Image.asset(
                  "assets/images/Vectorbg.png",
                  fit: BoxFit.cover,
                ),
              ),
              Positioned.fill(
                child: Image.asset(
                  "assets/images/Dodo.png",
                  alignment: Alignment.bottomCenter,
                ),
              ),
              Positioned(
                bottom: getProportionateScreenHeight(30),
                child: SizedBox(
                  width: Get.width,
                  height: getProportionateScreenHeight(100),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: getProportionateScreenWidth(70)),
                    child: TextFormField(
                      // textInputAction: Text,
                      controller: controller.searchController,
                      textInputAction: TextInputAction.search,
                      onFieldSubmitted: (value) {
                        // print(value);
                        Get.toNamed(Routes.BROWSER_SURFIOR, arguments: [
                          value,
                          child,
                        ]);
                        // Navigator.push(
                        //   context,
                        //   MaterialPageRoute(
                        //     builder: (context) => const BrowserSurfior(),
                        //     settings: RouteSettings(
                        //       arguments: value,
                        //     ),
                        //   ),
                        // );
                      },
                      decoration: InputDecoration(
                        suffixIcon: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(3),
                              vertical: getProportionateScreenHeight(1)),
                          child: GestureDetector(
                            onTap: () {},
                            child: Container(
                              color: kPrimaryColor,
                              child:
                                  Image.asset("assets/images/microphone.png"),
                            ),
                          ),
                        ),
                        hintText: "Type or Speak it. Kids!",
                        fillColor: kwhiteColor.withOpacity(0.7),
                        filled: true,
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                          color: kwhiteColor.withOpacity(0.7),
                        )),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: kwhiteColor.withOpacity(0.7),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
