import 'package:dodo_mobile/app/routes/app_pages.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LoginMoveToReportiorController extends GetxController {
  TextEditingController passwordController = TextEditingController();
  RxBool isLoading = false.obs;

  Future loginToReportior() async {
    final box = GetStorage();
    final data = box.read("dataParent") as Map<String, dynamic>;
    try {
      isLoading.value = true;
      if (passwordController.text == data["password"]) {
        Get.snackbar("Success", "Login Berhasil");
        clearPassword();
        isLoading.value = false;
        Get.offAndToNamed(Routes.REPORTIOR);
      } else {
        isLoading.value = true;
        Get.snackbar("Warning", "Password Salah");
        isLoading.value = false;
      }
    } catch (e) {
      isLoading.value = false;
      rethrow;
    }
  }

  void clearPassword() {
    passwordController.clear();
  }
}
