// ignore_for_file: unrelated_type_equality_checks

import 'package:dodo_mobile/app/modules/home/views/component_home/default_button.dart';
import 'package:dodo_mobile/app/utils/size_config.dart';
import 'package:dodo_mobile/app/utils/style.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

import '../controllers/login_move_to_reportior_controller.dart';

class LoginMoveToReportiorView extends GetView<LoginMoveToReportiorController> {
  const LoginMoveToReportiorView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(
                vertical: getProportionateScreenHeight(40),
                horizontal: getProportionateScreenWidth(32)),
            child: Column(
              children: [
                Image.asset(
                  "assets/images/Normal-Eye.png",
                  width: Get.width * 0.5,
                ),
                SizedBox(height: getProportionateScreenHeight(32)),
                Text(
                  "Hi parents, enter your password move to pages reportior!",
                  textAlign: TextAlign.center,
                  style: kTextStyleColor3.copyWith(
                    fontSize: getProportionateScreenWidth(45),
                  ),
                ),
                const Spacer(),
                TextFormField(
                  obscureText: true,
                  controller: controller.passwordController,
                  style: const TextStyle(color: Colors.white),
                  keyboardType: TextInputType.visiblePassword,
                  decoration: InputDecoration(
                    fillColor: kTextColor2,
                    filled: true,
                    hintText: "Enter your password",
                    hintStyle: const TextStyle(color: Colors.white),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: kTextColor2),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: kTextColor2),
                    ),
                  ),
                ),
                SizedBox(height: getProportionateScreenHeight(16)),
                Obx(
                  () => controller.isLoading == true
                      ? LottieBuilder.asset("assets/lottie/loading.json")
                      : DefaultButton(
                          text: "LOGIN",
                          color: kPrimaryColor,
                          press: () async {
                            await controller.loginToReportior();
                          }),
                ),
                const Spacer(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
