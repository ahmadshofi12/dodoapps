// ignore_for_file: unused_local_variable

import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dodo_mobile/app/data/models/child.dart';
import 'package:dodo_mobile/app/utils/constanta.dart';
import 'package:dodo_mobile/app/utils/style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;

class BrowserSurfiorController extends GetxController {
  String search = Get.arguments[0];
  // log(search);
  Child child = Get.arguments[1];
  // log(child.name.toString());
  final _endPoint = "log-activity/insert-data";

  final GlobalKey webViewKey = GlobalKey();
  InAppWebViewController? webViewController;
  late PullToRefreshController pullToRefreshController;
  String ulrs = "";
  double progress = 0;
  TextEditingController urlController = TextEditingController();
  List<ContentBlocker> blocker = [];

  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
    crossPlatform: InAppWebViewOptions(
      useShouldOverrideUrlLoading: true,
      mediaPlaybackRequiresUserGesture: true,
    ),
    android: AndroidInAppWebViewOptions(
      useHybridComposition: true,
    ),
    ios: IOSInAppWebViewOptions(
      useOnNavigationResponse: true,
    ),
  );

  sendNotification(String token, String name) async {
    try {
      log("Token Firebase" + token);
      Map<String, String> headerMap = {
        'Content-Type': 'application/json',
        'Authorization':
            'key=AAAAFzsTl28:APA91bENFgTQoDWCqYalO-gJN_t7dUJ0WXu1Nrf7r1bYvxe3xfUI7wxDgbzCwbWtsIQ5lK8KFadEOr0ILr9b1lZTbp6WDfEGSudN96xRjvaTKhxHpoFvD7tYsxeXBF-PzwiQUtljZzof',
      };
      String keywords = ulrs.split("=").last;
      Map notificationMap = {
        'body': 'Your Kids Access website $keywords',
        'title': child.name
      };
      Map sendNotificationMap = {
        "notification": notificationMap,
        "priority": "high",
        "to": token,
      };
      final respons = await http.post(
          Uri.parse("https://fcm.googleapis.com/fcm/send"),
          headers: headerMap,
          body: jsonEncode(sendNotificationMap));
    } catch (e) {}
  }

  Future readData(String email) async {
    FirebaseFirestore.instance
        .collection('user_parent')
        .where('emailParent', isEqualTo: email)
        .get()
        .then((QuerySnapshot querySnapshot) {
      querySnapshot.docs.forEach((element) {
        sendNotification(element['token'], element['childName']);
        log("Ini dijalankan berapa kali");
        print(element['emailParent']);
      });
    });
  }

  Future postUrl(String url) async {
    String keyword = url.split('=').last;
    log("Keyword" + keyword);
    final box = GetStorage();
    final data = box.read("dataParent") as Map<String, dynamic>;
    String accessToken = data["token"];
    try {
      final response =
          await http.post(Uri.parse(baseUrl + _endPoint), headers: {
        'Authorization': 'Bearer $accessToken'
      }, body: {
        'id_child': child.id.toString(),
        'web_title': keyword,
        'web_description': "'",
        'web_url': url,
        'status': "0",
      });
      readData(data['email']);
    } catch (e) {
      Get.snackbar("Gagal", "message");
      log(e.toString());
    }
  }

  @override
  void onInit() {
    // final proxy = CustomProxy(
    //             ipAddress:
    //                 "http://ec2-18-138-34-218.ap-southeast-1.compute.amazonaws.com/",
    //             port: 3128);
    //         proxy.enable();
    super.onInit();
    pullToRefreshController = PullToRefreshController(
        options: PullToRefreshOptions(
          color: kPrimaryColor,
        ),
        onRefresh: () async {
          if (Platform.isAndroid) {
            webViewController?.reload();
          } else if (Platform.isIOS) {
            webViewController?.loadUrl(
                urlRequest: URLRequest(url: await webViewController?.getUrl()));
          }
        });
  }
}
