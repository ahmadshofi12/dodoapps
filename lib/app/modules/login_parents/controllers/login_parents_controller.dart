import 'dart:convert';
import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dodo_mobile/app/routes/app_pages.dart';
import 'package:dodo_mobile/app/utils/constanta.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class LoginParentsController extends GetxController {
  late TextEditingController emailController = TextEditingController();
  late TextEditingController passwordController = TextEditingController();
  RxBool isLoading = false.obs;

  @override
  void onInit() async {
    await GetStorage.init();
    super.onInit();
  }

  @override
  void onClose() {
    emailController.dispose();
    passwordController.dispose();
    super.onClose();
  }

  Future getToken() async {
    String? token = await FirebaseMessaging.instance.getToken();
    log("TOKEN FIREBASE");
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('token_firebase', token!);
    log(token.toString());
  }

  Future sendDataToFirebase() async {
    final box = GetStorage();
    final prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('token_firebase');
    CollectionReference userParent =
        FirebaseFirestore.instance.collection("user_parent");

    return userParent
        .add({
          'emailParent': emailController.text,
          'childName': "",
          'token': token,
        })
        .then((value) => log("Success Save Data"))
        .catchError((err) => log("Failed Add Data"));
  }

  checkLogin(String token) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', token);
    log(token.toString());
  }

  Future loginParent() async {
    try {
      isLoading.value = true;
      final response =
          await http.post(Uri.parse("${baseUrl}auth/login"), body: {
        "email": emailController.text,
        "password": passwordController.text,
      });

      if (response.statusCode == 200) {
        Get.snackbar("Berhasil", "Login berhasil!!!");
        isLoading.value = false;
        log(json.decode(response.body)["data"].toString());
        var token = json.decode(response.body)["data"]["token"];
        // isAuth.value = true;
        await checkLogin(token);
        await getToken();
        await sendDataToFirebase();
        final box = GetStorage();
        box.write('dataParent', {
          'email': emailController.text,
          'password': passwordController.text,
          'token': token
        });
        Get.offAndToNamed(Routes.CHOOSE_USER);
      } else {
        isLoading.value = true;
        Get.snackbar("Gagal", "Login gagal!!!");
        isLoading.value = false;
      }
    } catch (err) {
      // isAuth.value = false;
      isLoading.value = false;
      // rethrow;
      log("Gagal Koneksi");
    }
  }
}
