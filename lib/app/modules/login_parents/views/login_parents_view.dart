// ignore_for_file: unrelated_type_equality_checks

import 'package:dodo_mobile/app/modules/home/views/component_home/default_button.dart';
import 'package:dodo_mobile/app/utils/size_config.dart';
import 'package:dodo_mobile/app/utils/style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import '../controllers/login_parents_controller.dart';

class LoginParentsView extends GetView<LoginParentsController> {
  const LoginParentsView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('LoginParentsView'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenHeight(20)),
            child: Column(
              children: [
                Image.asset("assets/images/Normal-Eye.png",
                    width: Get.width * 0.5),
                SizedBox(height: getProportionateScreenHeight(20)),
                Text(
                  "Hi parents, enter your email and password for login!",
                  textAlign: TextAlign.center,
                  style: kTextStyleColor3.copyWith(
                      fontSize: getProportionateScreenWidth(50)),
                ),
                // Spacer(),
                SizedBox(height: Get.height * 0.3),
                TextFormField(
                  style: const TextStyle(color: Colors.white),
                  keyboardType: TextInputType.emailAddress,
                  controller: controller.emailController,
                  decoration: InputDecoration(
                    fillColor: kPrimaryColor,
                    filled: true,
                    hintText: "Enter your email",
                    hintStyle: const TextStyle(color: Colors.white),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: kPrimaryColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: kPrimaryColor),
                    ),
                  ),
                ),
                SizedBox(height: getProportionateScreenHeight(20)),
                TextFormField(
                  obscureText: true,
                  controller: controller.passwordController,
                  style: const TextStyle(color: Colors.white),
                  keyboardType: TextInputType.visiblePassword,
                  decoration: InputDecoration(
                    fillColor: kPrimaryColor,
                    filled: true,
                    hintText: "Enter your password",
                    hintStyle: const TextStyle(color: Colors.white),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: kPrimaryColor),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: kPrimaryColor),
                    ),
                  ),
                ),
                SizedBox(height: getProportionateScreenHeight(20)),
                // DefaultButton(
                //     text: "GET TOKEN",
                //     color: kPrimaryColor,
                //     press: () => controller.getToken()),
                Obx(
                  () => controller.isLoading == true
                      ? LottieBuilder.asset("assets/lottie/loading.json")
                      : DefaultButton(
                          text: "LOGIN",
                          color: kPrimaryColor,
                          press: () async {
                            await controller.loginParent();
                          }),
                ),
                SizedBox(height: getProportionateScreenHeight(20)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
