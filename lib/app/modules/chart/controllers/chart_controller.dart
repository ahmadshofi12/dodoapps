import 'dart:math';

import 'package:get/get.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class ChartController extends GetxController {
  List<charts.Series<dynamic, String>> seriesList = [];

  static List<charts.Series<Sales, String>> _createRandomData() {
    final random = Random();
    final goodHistory = [
      Sales(year: '2015', sales: random.nextInt(100)),
      Sales(year: '2016', sales: random.nextInt(100)),
      Sales(year: '2017', sales: random.nextInt(100)),
      Sales(year: '2018', sales: random.nextInt(100)),
      Sales(year: '2019', sales: random.nextInt(100)),
      Sales(year: '2020', sales: random.nextInt(100)),
      Sales(year: '2021', sales: random.nextInt(100)),
      Sales(year: '2022', sales: random.nextInt(100)),
      Sales(year: '2023', sales: random.nextInt(100)),
      Sales(year: '2024', sales: random.nextInt(100)),
      Sales(year: '2025', sales: random.nextInt(100)),
      Sales(year: '2026', sales: random.nextInt(100)),
      Sales(year: '2027', sales: random.nextInt(100)),
    ];
    final badHistory = [
      Sales(year: '2015', sales: random.nextInt(100)),
      Sales(year: '2016', sales: random.nextInt(100)),
      Sales(year: '2017', sales: random.nextInt(100)),
      Sales(year: '2018', sales: random.nextInt(100)),
      Sales(year: '2019', sales: random.nextInt(100)),
      Sales(year: '2020', sales: random.nextInt(100)),
      Sales(year: '2021', sales: random.nextInt(100)),
      Sales(year: '2022', sales: random.nextInt(100)),
      Sales(year: '2023', sales: random.nextInt(100)),
      Sales(year: '2024', sales: random.nextInt(100)),
      Sales(year: '2025', sales: random.nextInt(100)),
      Sales(year: '2026', sales: random.nextInt(100)),
      Sales(year: '2027', sales: random.nextInt(100)),
    ];
    return [
      charts.Series<Sales, String>(
          id: 'Sales',
          domainFn: (Sales sales, _) => sales.year,
          measureFn: (Sales sales, _) => sales.sales,
          data: goodHistory,
          fillColorFn: (Sales sales, _) {
            return charts.MaterialPalette.green.shadeDefault;
          }),
      charts.Series<Sales, String>(
          id: 'Sales',
          domainFn: (Sales sales, _) => sales.year,
          measureFn: (Sales sales, _) => sales.sales,
          data: badHistory,
          fillColorFn: (Sales sales, _) {
            return charts.MaterialPalette.red.shadeDefault;
          })
    ];
  }

  barChart() {
    return charts.BarChart(
      seriesList,
      animate: true,
      vertical: false,
    );
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    seriesList = _createRandomData();
  }
}

class Sales {
  final String year;
  final int sales;

  Sales({required this.year, required this.sales});
}
