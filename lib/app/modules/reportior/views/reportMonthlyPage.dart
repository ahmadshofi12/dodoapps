import 'dart:developer';

import 'package:dodo_mobile/app/data/models/child.dart';
import 'package:dodo_mobile/app/modules/reportior/controllers/reportior_controller.dart';
import 'package:dodo_mobile/app/utils/size_config.dart';
import 'package:dodo_mobile/app/utils/style.dart';
import 'package:dropdown_search2/dropdown_search2.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class ReportMonthlyPage extends StatelessWidget {
  ReportMonthlyPage({Key? key}) : super(key: key);
  var controller = Get.find<ReportiorController>();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: getProportionateScreenHeight(10),
          horizontal: getProportionateScreenWidth(32)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Child`s Name Reviewed:",
            style: kTextStyleColor2.copyWith(
              fontSize: getProportionateScreenWidth(36),
              fontWeight: FontWeight.w500,
            ),
          ),
          SizedBox(height: getProportionateScreenHeight(10)),
          SizedBox(
            height: getProportionateScreenHeight(80),
            child: DropdownSearch<Child>(
              // maxHeight: 10,
              mode: Mode.MENU,
              dropdownSearchDecoration: InputDecoration(
                labelText: "Name",
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: kTextColor2,
                    width: 3,
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: kTextColor2,
                    width: 3,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: kTextColor2,
                    width: 3,
                  ),
                ),
              ),
              // showSearchBox: true,
              showClearButton: true,
              onFind: (text) => controller.getChilds(),
              popupItemBuilder: (context, item, isSelected) {
                return Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Text(
                    "${item.name}",
                    style: const TextStyle(fontSize: 18),
                  ),
                );
              },
              itemAsString: (item) => item!.name!,
              onChanged: (item) async {
                if (item != null) {
                  log(item.id.toString());
                  log(item.name.toString());
                  controller.idSearch.value = true;
                  controller.filterDataByMonthly(item.id.toString());
                } else {
                  controller.idSearch.value = false;
                  controller.filterDataByMonthly(0.toString());
                }
              },
            ),
          ),
          const SizedBox(height: 10),
          Container(
            width: Get.width,
            height: getProportionateScreenHeight(60),
            color: kTextColor2,
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(20)),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Kids Searching",
                  style: kTextStyleColor.copyWith(
                    fontSize: getProportionateScreenWidth(34),
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Obx(
              (() => SingleChildScrollView(
                    child: controller.founDataListPerMonthly.value.isEmpty
                        ? Padding(
                            padding: EdgeInsets.symmetric(
                                vertical: getProportionateScreenHeight(20)),
                            child: Center(
                                child: Text(
                              "Data Kosong",
                              style: kTextStyleColor3.copyWith(
                                fontSize: getProportionateScreenWidth(42),
                                fontWeight: FontWeight.w500,
                              ),
                            )),
                          )
                        : DataTable(
                            border:
                                TableBorder.all(color: kTextColor2, width: 1),
                            columns: const [
                              DataColumn(label: Text("Keyword")),
                              DataColumn(label: Text("Date")),
                              DataColumn(label: Text("Type")),
                            ],
                            rows: List.generate(
                                controller.founDataListPerMonthly.value.length,
                                (index) {
                              var data = controller
                                  .founDataListPerMonthly.value[index];
                              return DataRow(cells: [
                                DataCell(
                                  Text(
                                    data["web_title"].toString(),
                                    overflow: TextOverflow.clip,
                                    maxLines: 3,
                                  ),
                                ),
                                DataCell(
                                  Text(
                                    data["date"]
                                        .toString()
                                        .substring(0, 19)
                                        .split("T")
                                        .toString()
                                        .replaceAll("[", "")
                                        .replaceAll("]", ""),
                                    overflow: TextOverflow.clip,
                                    maxLines: 3,
                                  ),
                                ),
                                DataCell(
                                  data["status"] == 0.toString()
                                      ? Container(
                                          color: kPrimaryColor,
                                          height:
                                              getProportionateScreenHeight(20),
                                          width:
                                              getProportionateScreenHeight(20),
                                        )
                                      : Container(
                                          color: kRedColor,
                                          height:
                                              getProportionateScreenHeight(20),
                                          width:
                                              getProportionateScreenHeight(20),
                                        ),
                                ),
                              ]);
                            }),
                          ),
                  )),
            ),
          ),
        ],
      ),
    );
  }
}
