import 'package:dodo_mobile/app/data/models/child.dart';
import 'package:dodo_mobile/app/data/models/log_activity_model.dart';
import 'package:dodo_mobile/app/modules/home/views/component_home/default_button.dart';
import 'package:dodo_mobile/app/modules/reportior/bindings/reportior_binding.dart';
import 'package:dodo_mobile/app/modules/reportior/views/reportAllPage.dart';
import 'package:dodo_mobile/app/modules/reportior/views/reportDailyPage.dart';
import 'package:dodo_mobile/app/modules/reportior/views/reportMonthlyPage.dart';
import 'package:dodo_mobile/app/routes/app_pages.dart';
import 'package:dodo_mobile/app/utils/size_config.dart';
import 'package:dodo_mobile/app/utils/style.dart';
import 'package:dropdown_search2/dropdown_search2.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../controllers/reportior_controller.dart';

class ReportiorView extends GetView<ReportiorController> {
  ReportiorView({Key? key}) : super(key: key);
  var rController = Get.find<ReportiorController>();
  @override
  Widget build(BuildContext context) {
    DateTime time = DateTime.now();
    String dailyTime = DateFormat('dd-MMMM-yyyy').format(time);
    String monthlyTime = DateFormat('MMMM-yyyy').format(time);
    var idsearch = " ";
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kTextColor2,
        title: const Text('Reportior'),
        centerTitle: true,
        actions: [
          PopupMenuButton(
            itemBuilder: (contex) => [
              PopupMenuItem(
                value: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(
                      Icons.group_add,
                      color: kTextColor2,
                    ),
                    const Text("Add Child"),
                  ],
                ),
              ),
              PopupMenuItem(
                value: 2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(
                      Icons.stacked_line_chart_outlined,
                      color: kTextColor2,
                    ),
                    const Text("Chart"),
                  ],
                ),
              ),
              PopupMenuItem(
                value: 3,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(
                      Icons.settings,
                      color: kTextColor2,
                    ),
                    const Text("Setting"),
                  ],
                ),
              ),
              PopupMenuItem(
                value: 4,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(
                      Icons.exit_to_app,
                      color: kTextColor2,
                    ),
                    const Text("Logout"),
                  ],
                ),
              ),
            ],
            onSelected: (value) {
              if (value == 1) {
                Get.toNamed(Routes.LIST_CHILD);
              } else if (value == 2) {
                Get.toNamed(Routes.CHART);
                Get.snackbar("Setting", "Welcome to Chart");
              } else if (value == 3) {
                Get.toNamed(Routes.SETTINGS);
                Get.snackbar("Setting", "Welcome to Setting");
              } else if (value == 4) {
                controller.logout();
                Get.snackbar("Logout", "Success Logout");
              }
            },
          ),
        ],
      ),
      body: DefaultTabController(
        length: 3,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: getProportionateScreenHeight(10)),
            TabBar(
                indicatorColor: kPrimaryColor,
                labelColor: kTextColor2,
                tabs: [
                  Tab(
                    child: Text(
                      "ALL",
                      style:
                          TextStyle(fontSize: getProportionateScreenWidth(30)),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "Daily\n $dailyTime",
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontSize: getProportionateScreenWidth(27)),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "Monthly\n $monthlyTime",
                      textAlign: TextAlign.center,
                      style:
                          TextStyle(fontSize: getProportionateScreenWidth(30)),
                    ),
                  ),
                ]),
            SizedBox(height: getProportionateScreenHeight(10)),
            Expanded(
              child: TabBarView(children: [
                //page all
                ReportPageAll(),
                //page daily
                ReportDailyPage(),
                //page monthly
                ReportMonthlyPage(),
              ]),
            )
          ],
        ),
      ),
    );
  }
}
