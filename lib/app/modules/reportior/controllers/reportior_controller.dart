import 'dart:convert';
import 'dart:ffi';

import 'package:dodo_mobile/app/data/models/log_activity_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../data/models/child.dart';
import '../../../utils/constanta.dart';

class ReportiorController extends GetxController {
  //TODO: Implement ReportiorController
  // TabController tabController;

  final _endPointGetChild = "child/get-user";
  final _endPointLoadActivity = "log-activity/get-data";
  var models;
  RxBool idSearch = false.obs;
  List<dynamic> data = [];
  List<Map<String, dynamic>> dataListPerDay = [];
  List<Map<String, dynamic>> dataListPerMonthly = [];
  List<dynamic> hasilFilterByDay = [];
  List<dynamic> hasilFilterByMonth = [];
  Rx<List<dynamic>> founDataListPerDay = Rx<List<dynamic>>([]);
  Rx<List<dynamic>> founDataListPerMonthly = Rx<List<dynamic>>([]);
  Rx<List<dynamic>> foundData = Rx<List<dynamic>>([]);

  Future<List<Child>> getChilds() async {
    final box = GetStorage();
    final data = box.read("dataParent") as Map<String, dynamic>;
    String accessToken = data["token"];
    try {
      var response = await http.get(
          Uri.parse(
            baseUrl + _endPointGetChild,
          ),
          headers: {'Authorization': 'Bearer $accessToken'});
      if (response.statusCode != 200) {
        throw "error1";
      }

      var dataAnak = json.decode(response.body)["data"] as List<dynamic>;
      models = Child.fromJsonList(dataAnak);
    } catch (err) {
      print(err);
    }
    return models;
  }

  void showPopUpMenu() {
    Get.snackbar(
      "MENU",
      "message",
    );
  }

  Future<List<LogActivityModel>> loadLogActivity() async {
    final box = GetStorage();
    final dataStorage = box.read("dataParent") as Map<String, dynamic>;
    String accessToken = dataStorage["token"];

    try {
      var response = await http.get(
          Uri.parse(
            baseUrl + _endPointLoadActivity,
          ),
          headers: {'Authorization': 'Bearer $accessToken'});
      // print(response.body);
      if (response.statusCode != 200) {
        // print(response);
        throw "error1";
      }

      data = json.decode(response.body)["data"] as List<dynamic>;
      // print(data);
      models = LogActivityModel.fromJsonList(data);
      // print(models);
    } catch (err) {
      print(err);
    }
    return models;
  }

  @override
  void onInit() async {
    await GetStorage.init();
    // TODO: implement onInit
    await loadLogActivity();
    await getAllDataByDay();
    await getAllDataByMonth();
    super.onInit();
    founDataListPerMonthly.value = hasilFilterByMonth;
    founDataListPerDay.value = hasilFilterByDay;
    foundData.value = data;
  }

  Future getAllDataByMonth() async {
    final date = DateTime.now();
    final dateNow = date.toString().substring(5, 7);
    // print(dateNow);
    for (var i = 0; i < data.length; i++) {
      if (dateNow == data[i]["date"].toString().substring(5, 7)) {
        dataListPerMonthly.add({
          "id": data[i]["id"],
          "id_child": data[i]["id_child"],
          "web_title": data[i]["web_title"],
          "date": data[i]["date"],
          "status": data[i]["status"],
        });
      }
    }
    hasilFilterByMonth = dataListPerMonthly;
    return hasilFilterByMonth;
  }

  void filterDataByMonthly(String idChild) {
    var id = int.parse(idChild);
    List<dynamic> result = [];
    if ((idChild.isNotEmpty) && (idSearch.isTrue)) {
      result =
          hasilFilterByMonth.where((user) => user["id_child"] == id).toList();
    } else {
      print("sini");
      result = hasilFilterByMonth;
    }
    founDataListPerMonthly.value = result as List<dynamic>;
    models = LogActivityModel.fromJsonList(founDataListPerMonthly.value);

    return models;
  }

  Future getAllDataByDay() async {
    final date = DateTime.now();
    final dateNow = date.toString().substring(0, 10);
    for (var i = 0; i < data.length; i++) {
      if (dateNow == data[i]["date"].toString().substring(0, 10)) {
        dataListPerDay.add({
          "id": data[i]["id"],
          "id_child": data[i]["id_child"],
          "web_title": data[i]["web_title"],
          "date": data[i]["date"],
          "status": data[i]["status"],
        });
      }
    }
    hasilFilterByDay = dataListPerDay;
    return hasilFilterByDay;
  }

  void filterDataByDay(String idChild) {
    var id = int.parse(idChild);
    List<dynamic> result = [];
    if ((idChild.isNotEmpty) && (idSearch.isTrue)) {
      result =
          hasilFilterByDay.where((user) => user["id_child"] == id).toList();
    } else {
      print("sini");
      result = hasilFilterByDay;
    }
    founDataListPerDay.value = result as List<dynamic>;
    models = LogActivityModel.fromJsonList(founDataListPerDay.value);

    return models;
  }

  void filterData(String name) {
    var names = int.parse(name);
    List<dynamic> result = [];
    if ((name.isNotEmpty) && (idSearch.isTrue)) {
      result = data.where((user) => user["id_child"] == names).toList();
    } else {
      print("Tanpa Filter");
      result = data;
    }
    foundData.value = result as List<dynamic>;
    models = LogActivityModel.fromJsonList(foundData.value);
    print("INI MODELS");
    print(models);
    print("Data yang di Filter");
    print(foundData);
    return models;
  }

  void logout() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove('token');
    await prefs.remove('token_firebase');
  }
}
