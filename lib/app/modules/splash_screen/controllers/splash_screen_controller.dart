import 'dart:developer';

import 'package:dodo_mobile/app/routes/app_pages.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreenController extends GetxController {
  moveToHome() async {
    final prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('token');
    Future.delayed(const Duration(seconds: 3), () {
      if (token != null) {
        Get.offAllNamed(Routes.CHOOSE_USER);
      } else {
        Get.offAllNamed(Routes.HOME);
      }
    });
  }
}
