import 'package:dodo_mobile/app/utils/size_config.dart';
import 'package:dodo_mobile/app/utils/style.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/splash_screen_controller.dart';

class SplashScreenView extends GetView<SplashScreenController> {
  const SplashScreenView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: kPrimaryColor,
      body: FutureBuilder(
        future: controller.moveToHome(),
        builder: (context, snapshot) => SafeArea(
          child: Center(
            child: Column(
              children: [
                Spacer(),
                Text(
                  "WELCOME\nTO",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: getProportionateScreenWidth(70),
                    color: kwhiteColor,
                    fontWeight: FontWeight.w800,
                    fontStyle: FontStyle.italic,
                  ),
                ),
                Image.asset("assets/images/dodo white.png",
                    fit: BoxFit.cover,
                    height: getProportionateScreenHeight(150)),
                Spacer(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
