import 'dart:developer';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as htttp;

import '../../../utils/constanta.dart';

class RegisterParentController extends GetxController {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  RxBool isLoading = false.obs;

  final _endPoint = "parent/register-user";

  Future<bool> registerAccountParent() async {
    if (nameController.text.isNotEmpty) {
      if (emailController.text.isNotEmpty &&
          EmailValidator.validate(emailController.text)) {
        if (passwordController.text.isNotEmpty &&
            passwordController.text.length > 8) {
          try {
            isLoading.value = true;
            final response =
                await htttp.post(Uri.parse(baseUrl + _endPoint), body: {
              "name": nameController.text,
              "email": emailController.text,
              "password": passwordController.text,
              "create_at": DateTime.now().toString(),
              "update_at": "",
            });
            if (response.statusCode == 200) {
              Get.snackbar("Success", "Success Create Account Parent");
              clearTextField();
              isLoading.value = false;
              log(response.body);
              return true;
            } else {
              isLoading.value = true;
              log(response.body);
              Get.snackbar("Warning", "Create account fail!!");
              isLoading.value = false;
              return false;
            }
          } catch (response) {
            Get.snackbar("Warning", response.toString());
          }
        } else {
          Get.snackbar(
              "Warning", "Password tidak boleh kosong dan minimal 8 karakter ");
        }
      } else {
        Get.snackbar("Warning", "Format email tidak sesuai!!");
      }
    } else {
      Get.snackbar("Warning", "Nama Harus diisi ");
    }
    isLoading.value = false;
    return false;
  }

  void clearTextField() {
    nameController.clear();
    emailController.clear();
    passwordController.clear();
  }
}
