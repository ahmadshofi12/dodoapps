import 'package:dodo_mobile/app/modules/register_parent/views/component_register/input_textfield.dart';
import 'package:dodo_mobile/app/routes/app_pages.dart';
import 'package:dodo_mobile/app/utils/style.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

import '../../../utils/size_config.dart';
import '../../home/views/component_home/default_button.dart';
import '../controllers/register_parent_controller.dart';

class RegisterParentView extends GetView<RegisterParentController> {
  const RegisterParentView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
        backgroundColor: kwhiteColor,
        appBar: AppBar(
          leading: const Text(""),
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: SizeConfig.screenWidth! * 0.25,
                ),
                child: Image.asset("assets/images/Normal-Eye.png"),
              ),
              SizedBox(height: getProportionateScreenHeight(120)),
              Text(
                "Hi parents, enter your email and \npassword for register!",
                textAlign: TextAlign.center,
                style: kTextStyleColor2.copyWith(
                    fontSize: getProportionateScreenWidth(40)),
              ),
              SizedBox(height: SizeConfig.screenHeight! * 0.17),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(40)),
                child: Column(
                  children: [
                    InputTextField(
                      controller: controller.nameController,
                      textInputType: TextInputType.name,
                      hintText: "Enter Your Name",
                    ),
                    SizedBox(height: getProportionateScreenHeight(20)),
                    InputTextField(
                      hintText: "Enter Your Email",
                      controller: controller.emailController,
                      textInputType: TextInputType.emailAddress,
                    ),
                    SizedBox(height: getProportionateScreenHeight(20)),
                    TextFormField(
                      style: TextStyle(
                          color: kwhiteColor,
                          fontSize: getProportionateScreenWidth(32)),
                      controller: controller.passwordController,
                      obscureText: true,
                      decoration: InputDecoration(
                        fillColor: bgButtonColor,
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: bgButtonColor),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: bgButtonColor),
                        ),
                        hintText: "Enter your Password",
                        hintStyle: kTextStyleColor,
                      ),
                    ),
                    SizedBox(height: getProportionateScreenHeight(20)),
                    Obx(
                      // ignore: unrelated_type_equality_checks
                      () => controller.isLoading == true
                          ? LottieBuilder.asset("assets/lottie/loading.json")
                          : DefaultButton(
                              text: "Register",
                              press: () async {
                                await controller.registerAccountParent();
                              },
                              color: kPrimaryColor),
                    ),
                  ],
                ),
              ),
              SizedBox(height: getProportionateScreenHeight(20)),
            ],
          ),
        ),
      ),
    );
  }
}
