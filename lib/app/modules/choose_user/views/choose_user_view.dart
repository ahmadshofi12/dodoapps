import 'package:dodo_mobile/app/modules/home/views/component_home/default_button.dart';
import 'package:dodo_mobile/app/routes/app_pages.dart';
import 'package:dodo_mobile/app/utils/size_config.dart';
import 'package:dodo_mobile/app/utils/style.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/choose_user_controller.dart';

class ChooseUserView extends GetView<ChooseUserController> {
  const ChooseUserView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // var token = Get.arguments;
    return Scaffold(
      appBar: AppBar(
        title: const Text(''),
        centerTitle: true,
      ),
      body: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
        child: Column(
          children: [
            Image.asset(
              "assets/images/Normal-Eye.png",
              width: Get.width * 0.5,
            ),
            SizedBox(height: getProportionateScreenHeight(100)),
            Text(
              "Dodo",
              style: kTextStyleColor3.copyWith(
                  fontSize: Get.width * 0.1, fontWeight: FontWeight.w600),
            ),
            Text(
              "The magic Eyes Snail",
              style: kTextStyleColor3.copyWith(
                fontSize: Get.width * 0.06,
                fontWeight: FontWeight.w500,
              ),
            ),
            const Spacer(),
            DefaultButton(
                text: "PARENT",
                color: kPrimaryColor,
                press: () => Get.toNamed(Routes.LOGIN_MOVE_TO_REPORTIOR)),
            SizedBox(height: getProportionateScreenHeight(20)),
            DefaultButton(
                text: "KIDS",
                color: kPrimaryColor,
                press: () => Get.toNamed(Routes.CHOOSE_CHILD_NAME)),
            SizedBox(height: getProportionateScreenHeight(20)),
          ],
        ),
      ),
    );
  }
}
