class LogActivityModel {
  int? id;
  int? idChild;
  String? webTitle;
  String? webUrl;
  String? webDescription;
  String? status;
  String? date;
  String? createdAt;
  Null? updatedAt;
  UserChild? userChild;

  LogActivityModel(
      {this.id,
      this.idChild,
      this.webTitle,
      this.webUrl,
      this.webDescription,
      this.status,
      this.date,
      this.createdAt,
      this.updatedAt,
      this.userChild});

  LogActivityModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idChild = json['id_child'];
    webTitle = json['web_title'];
    webUrl = json['web_url'];
    webDescription = json['web_description'];
    status = json['status'];
    date = json['date'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    userChild = json['user_child'] != null
        ? new UserChild.fromJson(json['user_child'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_child'] = this.idChild;
    data['web_title'] = this.webTitle;
    data['web_url'] = this.webUrl;
    data['web_description'] = this.webDescription;
    data['status'] = this.status;
    data['date'] = this.date;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.userChild != null) {
      data['user_child'] = this.userChild!.toJson();
    }
    return data;
  }

  static List<LogActivityModel> fromJsonList(List list) {
    if (list.isEmpty) return List<LogActivityModel>.empty();
    return list.map((item) => LogActivityModel.fromJson(item)).toList();
  }
}

class UserChild {
  int? idParent;

  UserChild({this.idParent});

  UserChild.fromJson(Map<String, dynamic> json) {
    idParent = json['id_parent'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_parent'] = this.idParent;
    return data;
  }
}
