class Child {
  int? id;
  int? idParent;
  String? name;
  String? createdAt;
  Null? updatedAt;

  Child({this.id, this.idParent, this.name, this.createdAt, this.updatedAt});

  Child.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    idParent = json['id_parent'];
    name = json['name'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['id_parent'] = this.idParent;
    data['name'] = this.name;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }

  static List<Child> fromJsonList(List list) {
    if (list.isEmpty) return List<Child>.empty();
    return list.map((item) => Child.fromJson(item)).toList();
  }
}
